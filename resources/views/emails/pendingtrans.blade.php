<style>
    #customers {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #customers tr:nth-child(even){background-color: #f2f2f2;}

    #customers tr:hover {background-color: #ddd;}

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }
</style>
@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')
    <tr>
        <td class="title">
            ITC Pending Transactions
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">
            Pending Transaction for the past 1 hour -- ITC
        </td>
    </tr>

    <table id="customers">
        <tr>
            <th>Phone Number</th>
            <th>Transaction Id</th>
            <th>Date</th>
        </tr>

        @foreach($accountList as $account)
            <tr>
                <td>{{$account['phone_number']}}</td>
                <td>{{$account['transaction_id']}}</td>
                  <td>{{$account['date']}}</td>

            </tr>
            @endforeach
        <tr>
            <td colspan="2">
                @include('beautymail::templates.minty.button', ['text' => 'View', 'link' => ''])
            </td>
        </tr>
    </table>
    @include('beautymail::templates.minty.contentEnd')

@stop
