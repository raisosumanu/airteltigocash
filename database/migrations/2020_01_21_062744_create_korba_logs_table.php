<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKorbaLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('korba_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_id')->unique();
            $table->string('amount');
            $table->string('phone_number');
            $table->text('payload');
            $table->text('airtime_payload');
            $table->string('transaction_state')->default("pending");
            $table->text('airtime_result')->nullable();
            $table->string('airtime_failure_code')->nullable();
            $table->string('airtime_failure_reason')->nullable();
            $table->dateTime('result_received_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('korba_logs');
    }
}
