<?php

namespace App\Http\Controllers;

use App\KorbaLog;
use App\PendingTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class TransactionApiController extends BaseController
{
    //


    private $result_codes_messages = [
      "64000"=>"Idle subscriber on",
      "61334"=>"Amount is incorrect",
      "65000"=>"Subscriber does not exit",
      "20008"=>"Postpaid subscriber is not eligible",
      "61388"=>"Sorry, you cannot re-do this operation.",
      "64528"=>"Recharge will increase balance beyond max threshold",
      "64527"=>"Recharge will increase balance beyond max threshold",
      "61319"=>"Sorry the process failed. Please try again",
      "64528"=>"Recharge will increase balance beyond max threshold",

    ];

    public function airtimePurchase(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'korba_transationid' => 'required|unique:korba_logs,transaction_id',
            'amount' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'phone_number'=>'required|regex:/(233)[0-9]{9}/',

        ]);
        if ($validator->fails()) {
            return $this->sendError('Request Parameters Contain Errors',$validator->errors(),422);
        }

        $pin = env('PIN');
        $username = env('USERNAME');
        $password = env('PASSWORD');
        $dealer_number = env('DEALER_NUMBER');
        $client_id = env('CLIENT_ID');
        $amount=$request->amount*10000;



        $xml_request= '
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://xmlns.tigo.com/MFS/PurchaseInitiateRequest/V1" xmlns:v3="http://xmlns.tigo.com/RequestHeader/V3" xmlns:v2="http://xmlns.tigo.com/ParameterType/V2">
   <soapenv:Header xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
      <cor:debugFlag xmlns:cor="http://soa.mic.co.af/coredata_1">true</cor:debugFlag>
      <wsse:Security>
         <wsse:UsernameToken>
            <wsse:Username>live_mw_korba</wsse:Username>
            <wsse:Password>korba@123</wsse:Password>
         </wsse:UsernameToken>
      </wsse:Security>
   </soapenv:Header>
   <soapenv:Body>
      <v1:PurchaseInitiateRequest>
         <v3:RequestHeader>
            <v3:GeneralConsumerInformation>
               <v3:consumerID>Korba</v3:consumerID>
               <!--Optional:-->
               <v3:transactionID>'.$request->korba_transationid'.</v3:transactionID>
               <v3:country>GHA</v3:country>
               <v3:correlationID>*********</v3:correlationID>
            </v3:GeneralConsumerInformation>
         </v3:RequestHeader>
         <v1:requestBody>
            <v1:customerAccount>
               <!--You have a CHOICE of the next 2 items at this level-->
               <v1:msisdn>*************</v1:msisdn>
            </v1:customerAccount>
            <v1:initiatorAccount>
               <!--You have a CHOICE of the next 2 items at this level-->
               <v1:msisdn>*********</v1:msisdn>
            </v1:initiatorAccount>
            <v1:paymentReference>************</v1:paymentReference>
            <!--Optional:-->
            <v1:externalCategory>**********</v1:externalCategory>
            <!--Optional:-->
            <v1:externalChannel>********</v1:externalChannel>
            <!--Optional:-->
            <v1:webUser>***********</v1:webUser>
            <!--Optional:-->
            <v1:webPassword>********</v1:webPassword>
            <!--Optional:-->
            <v1:merchantName>*********</v1:merchantName>
            <!--Optional:-->
            <v1:merchantMsisdn>233261065081</v1:merchantMsisdn>
            <!--Optional:-->
            <v1:itemName>************</v1:itemName>
            <v1:amount>************</v1:amount>
            <!--Optional:-->
            <v1:minutesToExpire>60</v1:minutesToExpire>
            <v1:notificationChannel>1</v1:notificationChannel>
               <v1:AuthorizationCode>bGl2ZV9td19rb3JiYWtvcmJhQDEyMw==</v1:AuthorizationCode>
         </v1:requestBody>
      </v1:PurchaseInitiateRequest>
   </soapenv:Body>
</soapenv:Envelope>
        ';

        $airtime_apilog=new KorbaLog;
        $airtime_apilog->transaction_id=$request->korba_transationid;
        $airtime_apilog->payload=json_encode($request->all());
        $airtime_apilog->amount = $request->amount;
        $airtime_apilog->phone_number = $request->phone_number;
        $airtime_apilog->airtime_payload=Crypt::encryptString($xml_request);
        $airtime_apilog->save();

        $header = array(
              "Content-type: text/xml",
              "Accept: text/xml",
              "Accept-Encoding: gzip, deflate",
              "Cache-Control: no-cache",
              "Pragma: no-cache",
              "Content-length: ".strlen($xml_request),
          );
          $soap_do = curl_init();
          curl_setopt($soap_do, CURLOPT_URL,            env('REMOTE_URL') );
          curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
          curl_setopt($soap_do, CURLOPT_POST,           true );
          curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $xml_request);
          curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);
          $result = curl_exec($soap_do);
          //dd($result);
          //return $result;
          if($result === false) {
           $err = 'Curl error: ' . curl_error($soap_do);
           curl_close($soap_do);


           $sentdata=[
               "message"=>$err,
               "status"=>"failed"
           ];

           $airtime_apilog->update([
             'transaction_state'=>'failed',
             'airtime_failure_reason'=>$err,
             'airtime_result'=>$result,
             'result_received_at'=>date("Y-m-d H:i:s")
           ]);


           return $this->sendError('Transaction failed',$sentdata,422);

       }
       elseif (! $result) {
         curl_close($soap_do);
         $sentdata=[
             "message"=>"Error posting data",
             "status"=>"failed"
         ];

         $airtime_apilog->update([
           'transaction_state'=>'failed',
           'airtime_failure_reason'=>'Error posting data',
           'airtime_result'=>$result,
           'result_received_at'=>date("Y-m-d H:i:s")
         ]);

         return $this->sendError('Transaction failed',$sentdata,422);
       }

       else {
         curl_close($soap_do);
         $xml = simplexml_load_string($result);
         $response_message = $xml->children('soap', true)->Body->children()->RechargeExResponse;
         if( isset($response_message->children()->RechargeExResult[0]) && (string)$response_message->children()->RechargeExResult[0] == "success" )
         {
           // let us update the table


           $airtime_apilog->update([
             'transaction_state'=>'success',
             'airtime_result'=>$result,
             'result_received_at'=>date("Y-m-d H:i:s")
           ]);

           $sentdata=[
                     "message"=>"Operation was successful",
                     "status"=>"success"
                 ];

                 return $this->sendResponse($sentdata, 'Transaction  Successfully');
         }
         else {
           $error_code = trim((string)$response_message->children()->RechargeExResult[0]);
           $sentdata=[
               "message"=>$this->result_codes_messages[$error_code] ?? "No Error Code found",
               "status"=>"failed"
           ];

           $airtime_apilog->update([
             'transaction_state'=>'failed',
             'airtime_failure_reason'=>$this->result_codes_messages[$error_code] ?? "No Error Code found",
             'airtime_failure_code'=> $error_code ?? '',
             'airtime_result'=>$result,
             'result_received_at'=>date("Y-m-d H:i:s")
           ]);


           return $this->sendError('Transaction failed',$sentdata,422);

         }
         //dd($response_message);
       }

    }



    public function connectDb()
    {
        try {
            DB::connection()->getPdo();
            if(DB::connection()->getDatabaseName()){
                echo "Yes! Successfully connected to the DB: " . DB::connection()->getDatabaseName();
            }else{
                die("Could not find the database. Please check your configuration.");
            }
        } catch (\Exception $e) {
            die("Could not open connection to database server.  Please check your configuration.");
        }
    }

    public function connectenv()
    {
       dd(env('TET'));
    }

    public function connectenvaga()
    {
        dd(env('TEST_EN'));
    }


    //
}
