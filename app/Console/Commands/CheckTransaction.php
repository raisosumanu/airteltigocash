<?php

namespace App\Console\Commands;

use App\KorbaLog;
use App\PendingTransaction;
use Illuminate\Console\Command;

class CheckTransaction extends Command
{
    private static $status_check_codes = [
        "01"=>"SUCCESSFUL",
        "03"=>"PROCESSING",
        "131"=>"TIMEOUT",
        "99"=>"NOT FOUND",
        "529"=>"The customer does not have enough funds to complete or The transaction will cause the wallet to exceed the maximum amount it can hold and hence can't be completed",
        "527"=>"Number is not registered on mobile money",
        "515"=>"The MTN msisdn provided is not a registered subscriber",
        "682"=>"An internal error caused the operation to fail",
        "779"=>"Some other transactional operation is being performed on the wallet therefore this transaction can not be completed at this time"


    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:transaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking the status of transaction';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $pending_transactions = PendingTransaction::where('created_at', '<', \Carbon\Carbon::now()->subSeconds(2))->get();
        foreach ($pending_transactions as $pending_transaction)
        {
            self::checkStactus($pending_transaction->transaction_id);
        }

    }

    public static function checkStactus($transaction_id)
    {
        $receive_request = array(
            "merchantId"=>env('MERCHANT_ID'),
            "productId"=>env('PRODUCT_ID'),
            "apiKey"=> env('API_KEY')

        );
        $ch =  curl_init(env('REQUEST_URL_STATUS_CHECK').$transaction_id);
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($receive_request));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        //curl_setopt( $ch, CURLOPT_HTTPHEADER, $myauth);
        $result = curl_exec($ch);
        $results=json_decode($result,true);
        // dont do anything Transaction is still processing
        if(isset($results['responseCode']) && $results['responseCode']=="03")
        {

        }
        else
        {

            $response_mgs = self::$status_check_codes[$results['responseCode']] ?? "No error code";
            self::sendResultBack($response_mgs,$transaction_id,$results['responseCode']);
        }

    }

    // send feedback
    public static function sendResultBack($response_mgs,$transaction_id,$reasonCode)
    {
        $korbalogs=KorbaLog::where('transaction_id',$transaction_id)->first();
        if($korbalogs != null)
        {
            $korbalogs->update([
                'callback_flag'=>'1',
                'callback_state'=>$response_mgs

            ]);

            $post_message = [];
            if(isset($reasonCode) && $reasonCode == "01")
            {
                $post_message = [
                    "message"=>"Transaction Successful",
                    "status"=>"success",
                    "transaction_id"=>$transaction_id
                ];
            }
            else{
                $post_message = [
                    "message"=>"Transaction Failed",
                    "status"=>"failed",
                    "transaction_id"=>$transaction_id
                ];

            }

            $callback_url = $korbalogs->consumer_callback_url;



            self::sendCallbackMessage($callback_url,$post_message,$korbalogs);
        }

    }

    public static function sendCallbackMessage($url, array $post = NULL,$korbalogs, array $options = array())
    {

        $headers= array('Content-Type: application/json');
        $defaults = array(
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_URL => $url,
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_TIMEOUT => 4,
            CURLOPT_POSTFIELDS => json_encode($post),
            CURLOPT_HTTPHEADER =>$headers

        );

        $ch = curl_init();
        curl_setopt_array($ch, ($options + $defaults));
        if( ! $result = curl_exec($ch))
        {
            //trigger_error(curl_error($ch));
            $korbalogs->update([
                'consumer_callback_status'=>curl_error($ch)
            ]);
        }
        curl_close($ch);
        $korbalogs->update([
            'consumer_callback_status'=>'Callback Sent'
        ]);
        //return $result;
    }
}
