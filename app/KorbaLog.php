<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KorbaLog extends Model
{
    //
    protected $fillable = [
      'transaction_id',
      'amount',
      'phone_number',
      'payload',
      'airtime_payload',
      'transaction_state',
      'airtime_failure_code',
      'airtime_result',
      'airtime_failure_reason',
      'result_received_at'
    ];
}
