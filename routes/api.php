<?php

use Illuminate\Http\Request;

//use Snowfire;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('test-db','TransactionApiController@connectDb');

Route::get('test-env','TransactionApiController@connectenv');

Route::get('test-envd','TransactionApiController@connectenvaga');


Route::post('airtime_purchase','TransactionApiController@airtimePurchase');
